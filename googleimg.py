#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File: googleimg.py

# Time-stamp: <Coeus Wang: 2015-08-13 01:01:49>


import socket
import math
import urllib.request
import json
from optparse import OptionParser
import sys
from os import sep
from os import path
from os import makedirs
import requests
import re


socket.setdefaulttimeout(15)
base_num = 4
reqtosec = 15


def check_url_valid(url, valid_URL_list):
    # for debug purpose
    # if 'wordpress' in url:
    #     print("Stop")
    print(". -> Checking URL: " + url + "...")
    try:
        urlhead = requests.head(url, timeout=reqtosec)
        if urlhead.status_code == 200 and urlhead.is_redirect is False:
            if 'content-type' in dict(urlhead.headers).keys():
                if 'image' in urlhead.headers['content-type']:
                    print("\t:) valid URL: " + url)
                    valid_URL_list.append(url)
                else:
                    print("\t:( Not a valid URL(Not an image content): " +
                          url)
        elif urlhead.is_redirect is True:
            try:
                redirectURL = urllib.request.urlopen(url).geturl()
                if requests.head(redirectURL).status_code == 200 and \
                   'image' in \
                   requests.head(redirectURL).headers['content-type']:
                    print("\t:) Valid redirected URL: " + redirectURL +
                          " from original URL: " + url)
                    valid_URL_list.append(redirectURL)
                else:
                    print("\t:( Not a valid redirected URL" +
                          "(" + str(requests.head(redirectURL).status_code) +
                          ": " + requests.status_codes._codes[
                              requests.head(redirectURL).status_code
                          ][0] + "): " + redirectURL)
            except urllib.error.URLError:
                print("\t:( Not a valid redirected URL, No redirected URL.")
        else:
            print("\t:( Not a valid URL" +
                  "(" + str(requests.head(url).status_code) +
                  ": " + requests.status_codes._codes[
                      requests.head(url).status_code
                  ][0] + "): " + url)
    except:
        print("\t:( Not a valid URL(Server not found): " + url)


def get_image_links(qty, kw):

    """Get Real image Links"""

    startIndex = 0
    real_img_URLs = []
    idx = 0
    j = 0

    kw = re.sub("\s+", "+", kw)
    for i in range(1, math.ceil(int(qty) / base_num) + 1):
        url = 'http://ajax.googleapis.com/ajax/services/search/' + \
              'images?v=1.0&q=' + kw + "&start=" + str(startIndex)
        startIndex += base_num
        fetcher = urllib.request.build_opener()
        try:
            jsonraw = fetcher.open(url).read().decode('utf-8', 'ignore')
        except UnicodeEncodeError:
            jsonraw = fetcher.open(url).read().decode('utf-8').\
                      encode('cp850', 'replace').decode('cp850')
        jsondata = json.loads(jsonraw)

        if len(jsondata) > 0:
            for img in jsondata['responseData']['results']:
                if 'url' in img.keys():
                    if idx < int(qty):
                        if img['url'] not in real_img_URLs:
                            j += 1
                            idx += 1
                            print(str(j), end="")
                            check_url_valid(img['url'], real_img_URLs)
                            # real_img_URLs.append(img['url'])
                    else:
                        break
                else:
                    continue

    return real_img_URLs


def saveImgFile(img_real_URLs, img_store_path):
    hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) ' +
                         'AppleWebKit/537.11 (KHTML, like Gecko) ' +
                         'Chrome/23.0.1271.64 Safari/537.11',
           'Accept': 'text/html,application/xhtml+xml,application/xml;' +
                     'q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}
    i = 0
    number = 0
    if 0 < len(img_real_URLs) < 1000:
        number = 3
    else:
        number = 4

    prefix = "google_"

    for url in img_real_URLs:
        i += 1
        # For debug purpose
        # if 'pngimg' in url:
        #     print("Stop")
        print(str(i) + '.' + "Now try to download: " + url)
        if len(url.split('.')[-1]) > 5:
            file_ext = requests.head(url).\
                       headers['content-type'].split('/')[-1]
            savename = prefix + str(i).zfill(number) + '.' + file_ext
        else:
            savename = prefix + str(i).zfill(number) + \
                       '.' + url.split('.')[-1].lower()
        try:
            urllib.request.urlretrieve(url,
                                       img_store_path + sep + savename)
            print("\t:) Saved to: " +
                  path.dirname(path.realpath(__file__)) + sep +
                  img_store_path.replace('.\\', '') + sep + savename)
        except urllib.error.HTTPError:
            req = urllib.request.Request(url, None, hdr)
            response = urllib.request.urlopen(req)
            savefile = open(img_store_path + sep + savename, 'wb')
            savefile.write(response.read())
            savefile.close()
            print("\t:) Saved to: " +
                  path.dirname(path.realpath(__file__)) + sep +
                  img_store_path.replace('.\\', '') + sep + savename)
        except:
            print('\t:( Time out for URL: ', url)


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-w", "--keyword", default=None, dest="keyword",
                      help="Input a key word for search, no whitespace\
                      allowed. like: -w Spring")
    parser.add_option("-n", "--quantity", default=4, dest="quantity",
                      help="Input a number to indicate how many images\
                      to get. like: -n 50")
    parser.add_option("-p", "--store_path", default=".", dest="storepath",
                      help="Input a path where the picture can be saved\
                      like: -p C:\\temp")
    (options, args) = parser.parse_args()

    if options.keyword is None:
        print("No keyword assigned. Use \"-w keyword\"" +
              "as augment for image search.")
        sys.exit(0)

    if options.storepath == '.':
        options.storepath = '.' + sep + options.keyword
    if not path.isdir(options.storepath):
        makedirs(options.storepath)

    img_urls = get_image_links(options.quantity, options.keyword)
    print('-' * 10 + "Now begin downloading" + '-' * 10)
    saveImgFile(img_urls, options.storepath)
