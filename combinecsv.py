#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File: combinecsv.py

# Time-stamp: <Coeus Wang: 2015-08-25 01:15:32>


import csv
import os
from os import sep
from os import listdir
from os.path import abspath
import re
from optparse import OptionParser


def AddDate(rootpath):
    allyearfolder = []
    for rootdir, subdir, filename in os.walk(rootpath):
        for sd in subdir:
            if re.match('\d{4}', sd):
                allyearfolder.append(abspath(rootdir) + sep + sd)

    roots = []
    all_list = []
    full_list = []
    y = 0
    num = 0
    for yearpath in sorted(allyearfolder):
        year = yearpath.split(sep)[-1]
        rootpath = yearpath.replace(year, '')

        if rootpath not in roots:
            num = sum(1 for y in allyearfolder if rootpath in y)
            roots.append(rootpath)

        csvfiles = [abspath(yearpath) + sep + f for f in listdir(yearpath)
                    if re.match(r'\d\d\.csv', f)]
        outputall = open(rootpath + sep + year + '_all.csv',
                         'w', encoding='gbk')
        csvfall = csv.writer(outputall, lineterminator='\n',)
        all_list = []

        for cf in sorted(csvfiles):
            print("Now processing " + cf)
            month = cf.split(sep)[-1].split('.')[0]
            with open(cf, encoding='gbk') as inputcsv,\
                open(yearpath + sep + 'date_' + month + '.csv', 'w',
                     encoding='gbk') as outputcsv:
                csvfi = csv.reader(inputcsv)
                csvfo = csv.writer(outputcsv, lineterminator='\n',)
                mylist = []
                header = next(csvfi)
                i = 0
                for row in csvfi:
                    i += 1
                    # print(i)
                    row.insert(0, year + month)
                    mylist.append(row)
                header.insert(0, '日期')
                if header not in all_list:
                    all_list.insert(0, header)
                mylist.insert(0, header)
                csvfo.writerows(mylist)
                all_list += mylist[1:]
        csvfall.writerows(all_list)

        if rootpath in yearpath:
            y += 1
            if y > 1:
                full_list += all_list[1:]
            else:
                full_list += all_list
            if y == num:
                outputfull = open(rootpath + sep + 'full_data.csv',
                                  'w', encoding='gbk')
                csvffull = csv.writer(outputfull, lineterminator='\n',)
                csvffull.writerows(full_list)
                outputfull.close()
                full_list = []
                y = 0

        if len(csvfiles) == 0:
            print("There is no file under " + yearpath)
        else:
            print("\nAll csv file under " + yearpath + " have been combined.")

    return allyearfolder


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-d", "--rootpath", default='.' + sep, dest="rootpath",
                      help="Input a rootpath contains csv files.")
    (options, args) = parser.parse_args()

    AddDate(options.rootpath)
