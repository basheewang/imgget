#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File: baiduimg.py

# Time-stamp: <Wang, Chen: 2015-05-26 13:28:39>

import http.cookiejar
import urllib.request
# from bs4 import BeautifulSoup
# import re
import json
import requests
from optparse import OptionParser
import sys
from os import sep
from os import path
from os import makedirs
import socket
import math

'''
自动抓取百度图片搜索结果以及保存图片到指定文件夹
'''

socket.setdefaulttimeout(15)


class BaiduImage:

    """
    This class defines all avaiable functions to parse baidu image search.
    """

    image_links = []
    current_page = ''
    base_num = 60
    timeoutsec = 15

    marks = {
        '_z&e3B': '.',
        'AzdH3F': '/',
        '_z2C$q': ':',
    }
    decodedict = {
        'o': 'w',
        'v': 'c',
        'g': 'n',
        '5': 'o',
        '4': 'm',
        'i': 'h',
        'p': 't',
        'r': 'p',
        'j': 'e',
        'f': 's',
        '8': '1',
        '0': '7',
        'l': '9',
        'x': 'x',
        't': 'i',
        'z': 'z',
        'w': 'a',
        '2': 'g',
        '7': 'u',
        'g': 'n',
        'b': '8',
        '6': 'r',
        'k': 'b',
        'c': '5',
        'm': '6',
        'y': 'y',
        'n': '3',
        'q': 'q',
        '1': 'd',
        'a': '0',
        'u': 'f',
        '3': 'j',
        's': 'l',
        'e': 'v',
        '9': '4',
        'd': '2',
        'h': 'k'
    }

    def __init__(self, current_page):
        self.current_page = current_page
        self.cj = http.cookiejar.LWPCookieJar()
        try:
            self.cj.revert('baiduimage.cookie')
        except:
            None
        self.opener = urllib.request.build_opener(
            urllib.request.HTTPCookieProcessor(self.cj))
        urllib.request.install_opener(self.opener)
        self.opener.addheaders = [
            ("User-agent",
             "Mozilla/5.0 (X11; U; FreeBSD i386; en-US; rv:1.9.1)" +
             "Gecko/20090704 Firefox/3.5"),
            ("Accept", "*/*")
        ]

    def get_image_links(self, qty, kw):
        """ 得到当前页面中图片的链接地址 """

        objURLs = []
        img_real_URLs = []
        for i in range(1, math.ceil(int(qty) / self.base_num) + 1):
            start_page = 'http://image.baidu.com/i?tn=resultjson_com' + \
                         '&ipn=rj&ct=201326592&is=&fp=result' + \
                         '&queryWord=' + urllib.request.quote(kw) + \
                         '&cl=2&lm=-1&ie=utf-8&oe=utf-8&adpicid=&st=-1' + \
                         '&z=&ic=0&word=' + urllib.request.quote(kw) + \
                         '&s=&se=1&tab=&width=&height=&face=0&istype=2' + \
                         '&qc=&nc=1&fr=%26fr%3D&pn=' + str(i * 60) + \
                         '&rn=60&1432190251700='
            try:
                html = self.opener.open(start_page).read()
            except Exception as e:
                self.write_log(e)
                return
            html = html.decode('utf-8', 'ignore')
            # fh = open('web.html', mode='w', encoding='utf-8')
            # print(html, file=fh)

            imgdata = json.loads(html)['data']
            for img in imgdata:
                if 'objURL' in img.keys():
                    objURL = img['objURL']
                else:
                    continue
                decodeURL = ''
                for k, v in self.marks.items():
                    objURL = objURL.replace(k, v)
                for c in objURL:
                    if c in self.decodedict.keys():
                        decodeURL += self.decodedict[c]
                    else:
                        decodeURL += c
                if decodeURL not in objURLs:
                    objURLs.append(decodeURL)

        j = 0
        if len(objURLs) > int(qty):
            objURLs = objURLs[0:int(qty)]

        for url in objURLs:
            j += 1
            print(str(j) + ". -> Checking URL: " + url + "...")
            try:
                urlhead = requests.head(url, timeout=self.timeoutsec)
            except:
                print("\t:( Not a valid URL(Server not found): " + url)
                continue
            if urlhead.status_code == 200 and urlhead.is_redirect is False:
                if 'content-type' in dict(urlhead.headers).keys():
                    if 'image' in urlhead.headers['content-type']:
                        print("\t:) valid URL: " + url)
                        img_real_URLs.append(url)
                else:
                    print("\t:( Not a valid URL(Not an image content): " +
                          url)
            elif urlhead.status_code == 200 and urlhead.is_redirect is True:
                redirectURL = urllib.request.urlopen(url).geturl()
                if requests.head(redirectURL, timeout=self.timeoutsec).\
                   status_code == 200 and 'image' in \
                   requests.head(redirectURL, timeout=self.timeoutsec).\
                   headers['content-type']:
                    print("\t\:) Valid redirected URL: " + redirectURL +
                          " from original URL: " + url)
                    img_real_URLs.append(redirectURL)
                else:
                    print("\t:( Not a valid redirected URL" +
                          "(" + str(requests.head(redirectURL).status_code) +
                          ": " + requests.status_codes._codes[
                              requests.head(redirectURL).status_code
                          ][0] + "): " + redirectURL)
            else:
                print("\t:( Not a valid URL" +
                      "(" + str(requests.head(url).status_code) +
                      ": " + requests.status_codes._codes[
                          requests.head(url).status_code
                          ][0] + "): " + url)

        return img_real_URLs

    def saveImgFile(self, img_real_URLs, img_store_path):
        i = 0
        number = 0
        if 0 < len(img_real_URLs) < 1000:
            number = 3
        else:
            number = 4

        prefix = "baidu_"
        for url in img_real_URLs:
            i += 1
            print(str(i) + '.' + "Now try to download: " + url)
            if len(url.split('.')[-1]) > 5:
                file_ext = requests.head(url).\
                           headers['content-type'].split('/')[-1]
                savename = prefix + str(i).zfill(number) + '.' + file_ext
            else:
                savename = prefix + str(i).zfill(number) + \
                           '.' + url.split('.')[-1].lower()
            try:
                urllib.request.urlretrieve(url,
                                           img_store_path + sep + savename)
                print("\t:) Saved to: " + savename)
            except:
                print('\t:( Time out for URL: ', url)

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-w", "--keyword", default=None, dest="keyword",
                      help="Input a key word for search, no whitespace\
                      allowed. like: -w Spring")
    parser.add_option("-n", "--quantity", default=60, dest="quantity",
                      help="Input a number to indicate how many images\
                      to get. like: -n 50")
    parser.add_option("-p", "--store_path", default=".", dest="storepath",
                      help="Input a path where the picture can be saved\
                      like: -p C:\\temp")
    (options, args) = parser.parse_args()

    if options.keyword is None:
        print("No keyword assigned. Use \"-w keyword\"" +
              "as augment for image search.")
        sys.exit(0)

    if options.storepath == '.':
        options.storepath = '.\\' + options.keyword
    if not path.isdir(options.storepath):
        makedirs(options.storepath)

    # options.keyword = '然乌湖'
    start_page = 'http://image.baidu.com/i?tn=baiduimage' + \
                 '&ipn=r&ct=201326592&cl=2&lm=-1&st=-1' + \
                 '&fm=result&fr=&sf=1&fmq=1431590677977_R' + \
                 '&pv=&ic=0&nc=1&z=&se=1&showtab=0&fb=0' + \
                 '&width=&height=&face=0&istype=2&ie=utf-8' + \
                 '&word=' + urllib.request.quote(options.keyword)

    app = BaiduImage(current_page=start_page)
    img_valid_urls = app.get_image_links(options.quantity,
                                         options.keyword)
    print('-' * 10 + "Now begin downloading" + '-' * 10)
    app.saveImgFile(img_valid_urls, options.storepath)
